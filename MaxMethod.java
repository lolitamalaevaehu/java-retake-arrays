package com.epam.rd.autotasks.max;

import java.util.Scanner;

public class MaxMethod {
    public static int max(int[] values) {
        int max = values[0];
        for (int i = 1; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }
        return max;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] values = new int[5];
        for (int i = 0; i < values.length; i++) {
            values[i] = scan.nextInt();
        }
        System.out.println(max(values));
    }
}

